# shopping_app

A new Flutter application.

## Getting Started

For help getting started with Flutter, view our online [documentation](https://flutter.io/).

## Design

The app design is managed on [Figma](https://www.figma.com/file/Et23TjnubKT0VBeyUM9mdBpW/Shopping-App?node-id=4%3A14). App should be designed according to the figma project.