import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class HistoricalBills extends StatelessWidget {
  final String _userID;
  HistoricalBills(this._userID);

  @override
  Widget build(BuildContext context) {
    final CollectionReference historyRef = Firestore.instance
        .collection('user')
        .document(_userID)
        .collection('history');

    return new StreamBuilder<QuerySnapshot>(
      stream: historyRef.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) {
          return new Center(
            child: new CircularProgressIndicator(),
          );
        }
        return new ListView(
          children: snapshot.data.documents.map((DocumentSnapshot document) {
            return new ListTile(
              leading: new Icon(Icons.description),
              title: new Text('TODO'),
              subtitle: new Text(document['TODO']),
            );
          }).toList(),
        );
      },
    );
  }
}
