import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

final FirebaseAuth _auth = FirebaseAuth.instance;

class Shops extends StatefulWidget {
  @override
  _ShopsState createState() => new _ShopsState();
}

class _ShopsState extends State<Shops> with SingleTickerProviderStateMixin {
  Map<String, Shop> _savedShops;
  List<Shop> _shopList;
  FirebaseUser _user;
  final httpClient = new http.Client();
  TabController controller;

  final String url =
      "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
          "radius=600" +
          "&type=supermarket" +
          "&key=AIzaSyA5vzgD59OnVdoOEqnIZNRopYJySMhRYqo" +
          "&location=51.3377804,12.3768377";

  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    fetchNearbyShops(httpClient, url);
    _updateUser();

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Märkte'),
        bottom: new TabBar(
          controller: controller,
          tabs: [
            new Tab(
              icon: new Icon(Icons.favorite),
              text: "Favoriten",
            ),
            new Tab(
              icon: new Icon(Icons.my_location),
              text: "In der Nähe",
            ),
          ],
        ),
      ),
      body: new TabBarView(
        controller: controller,
        children: [
          viewFavorites(),
          viewAll(),
        ],
      ),
    );
  }

  Container viewFavorites() {
    return new Container(
      padding: new EdgeInsets.all(8.0),
      child: buildFavoriteShops(),
    );
  }

  Container viewAll() {
    return new Container(
      padding: new EdgeInsets.all(8.0),
      child: buildNearbyShops(),
    );
  }

  buildNearbyShops() {
    return new FutureBuilder(
        future: fetchNearbyShops(httpClient, url),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            _shopList = snapshot.data;
            //return new ListView(children: assembleNearbyTiles());
          } else if (snapshot.hasError) {
            print("${snapshot.error}");
          }

          if (_shopList != null) {
            if (_shopList.isEmpty) {
              return new Center(
                child: new Text('Keine Märkte in der Nähe gefunden.'),
              );
            }
            return new ListView(children: assembleNearbyTiles());
          }
          // By default, show a loading spinner
          return new Center(child: new CircularProgressIndicator());
        });
  }

  Widget buildFavoriteShops() {
    if (_user == null) {
      return new Center(child: new CircularProgressIndicator());
    }

    CollectionReference shopsRef = Firestore.instance
        .collection('user')
        .document(_user.uid)
        .collection('fav-shops');

    return new StreamBuilder(
      stream: shopsRef.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) {
          // By default, show a loading spinner
          return new Center(child: new CircularProgressIndicator());
        }
        if (snapshot.hasData) {
          final list = snapshot.data.documents
              .map<Shop>(
                  (DocumentSnapshot document) => Shop.fromJson(document.data))
              .toList();
          _savedShops = new Map<String, Shop>.fromIterable(list,
              key: (item) => item.placeId, value: (item) => item);
        } else if (snapshot.hasError) {
          print("${snapshot.error}");
        }

        if (_savedShops != null) {
          if (_savedShops.isEmpty) {
            return new Center(
              child: new MaterialButton(
                color: Colors.green,
                child: new Text(
                  "Märkte in der Nähe",
                  style: new TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  //TODO: Open tab 2
                  controller.animateTo(1);
                },
              ),
            );
          }
          return new ListView(
            children: _savedShops.values
                .map((Shop shop) => _buildShopTile(shop))
                .toList(),
          );
        }
      },
    );
  }

  List<ListTile> assembleNearbyTiles() {
    List<ListTile> tiles = new List();
    _shopList.forEach((shop) => tiles.add(_buildShopTile(shop)));
    return tiles;
  }

  ListTile _buildShopTile(Shop shop) {
    final bool alreadySaved = _savedShops.containsKey(shop.placeId);
    return new ListTile(
      leading: new IconButton(
        icon: new Icon(
          alreadySaved ? Icons.favorite : Icons.favorite_border,
          color: alreadySaved ? Colors.redAccent : Colors.grey,
        ),
        onPressed: () {
          DocumentReference shopRef = Firestore.instance
              .collection('user')
              .document(_user.uid)
              .collection('fav-shops')
              .document(shop.placeId);
          setState(() {
            if (alreadySaved) {
              _savedShops.remove(shop.placeId);
              shopRef.delete();
            } else {
              _savedShops[shop.placeId] = shop;
              shopRef.updateData(shop.toJson());
            }
          });
        },
      ),
      title: new Text(shop.name),
      subtitle: new Text(shop.address),
      onTap: () {
        //TODO: öffne "Einkaufen" für diesen Markt
        // Navigator.pop(context); // -> drawer
      },
    );
  }

  Future<Null> _updateUser() async {
    if (_user == null) {
      await _auth.signInAnonymously();
    }
    final FirebaseUser user = await _auth.currentUser();
    setState(() {
      _user = user;
    });
  }

  Future<List<Shop>> fetchNearbyShops(http.Client client, String url) async {
    final response = await client.get(url);
    final responseJson = json.decode(response.body);
    final List results = responseJson['results'];
    if (results == []) {
      String status = responseJson['status'];
      String msg = responseJson['error_message'];
      print(status + ': ' + msg);
      return [];
    }
    return results.map<Shop>((result) => Shop.fromJson(result)).toList();
  }
}

class Shop {
  final String name;
  final String address;
  final String placeId;

  Shop({this.name, this.address, this.placeId});

  factory Shop.fromJson(Map<String, dynamic> json) {
    return new Shop(
        name: json['name'],
        address: json['vicinity'],
        placeId: json['place_id']);
  }

  Map<String, dynamic> toJson() {
    return {"name": name, "vicinity": address, "place_id": placeId};
  }
}
