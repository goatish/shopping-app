import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn _googleSignIn = new GoogleSignIn();

class SignInPage extends StatelessWidget {
  Future<Null> _signInWithGoogle(BuildContext context) async {
    final FirebaseUser oldUser = await _auth.currentUser();
    final String oldUid = oldUser.uid;
    QuerySnapshot oldCart;

    if (oldUser.isAnonymous) {
      oldCart = await getUserData(oldUid);
    }

    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    await _auth.signInWithGoogle(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    if (oldUser.isAnonymous) {
      // get the new user
      final FirebaseUser newUser = await _auth.currentUser();
      final String newUid = newUser.uid;

      // add old cart items
      await setUserData(oldCart, newUid);
    }

    Navigator.of(context).pop();
  }

  Future<Null> _signInWithFacebook(BuildContext context) async {}

  Future<Null> _signInWithEmail(BuildContext context) async {}

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Anmelden'),
      ),
      backgroundColor: Colors.grey.shade900,
      body: new Center(
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new SignInButton(
                  _signInWithGoogle,
                  new Text(
                    "G",
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24.0,
                        color: Colors.white),
                  ),
                  "Mit Google anmelden",
                  Colors.red),
              new SignInButton(
                  _signInWithFacebook,
                  new Text(
                    "f",
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24.0,
                        color: Colors.white),
                  ),
                  "Mit Facebook anmelden",
                  Colors.blue.shade900),
              new SignInButton(
                  _signInWithEmail,
                  new Icon(
                    Icons.mail,
                    color: Colors.white,
                  ),
                  "Mit Email anmelden",
                  Colors.orange),
            ]),
      ),
    );
  }

  Future<Null> setUserData(QuerySnapshot oldCart, String newUid) async {
    // add old cart items
    for (DocumentSnapshot doc in oldCart.documents) {
      await Firestore.instance
          .collection('user')
          .document(newUid)
          .collection('cart')
          .add(doc.data);
    }
  }

  Future<QuerySnapshot> getUserData(String oldUid) async {
    DocumentReference oldUserRef =
        Firestore.instance.collection('user').document(oldUid);
    final QuerySnapshot oldCart =
        await oldUserRef.collection('cart').getDocuments();

    //delete old user
    oldUserRef.setData({'delete_me': true});

    return oldCart;
  }
}

class SignInButton extends StatelessWidget {
  final signIn;
  final Widget icon;
  final String text;
  final Color color;

  SignInButton(this.signIn, this.icon, this.text, this.color);

  @override
  Widget build(BuildContext context) {
    return new Container(
        margin: new EdgeInsets.only(bottom: 20.0),
        width: 210.0,
        child: new RaisedButton(
          onPressed: () {
            signIn(context);
          },
          color: color,
          child: new Container(
              child: new Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Padding(
                padding: new EdgeInsets.fromLTRB(0.0, 8.0, 16.0, 8.0),
                child: icon,
              ),
              new Text(text, style: new TextStyle(color: Colors.white))
            ],
          )),
        ));
  }
}
