import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

main() async {
  final barcode = "4002293401102";
  //final barcode = "4311596459902";
  var result = await UpcItemDB.fetchItemDatabase(barcode);
  print(result);
}

class UpcItemDB {
  static final String baseUrl = "https://api.upcitemdb.com/prod";
  static final String trialUrl = baseUrl + "/trial";
  static final String paidUrl = baseUrl + "/v1";
  bool trial = true;

  static Future fetchItemDatabase(String barcode) async {
    final url = UpcItemDB.trialUrl + "/lookup?upc=$barcode";
    http.Response httpResponse =
        await http.get(url, headers: {"Accept": "application/json"});
    if (httpResponse.statusCode == 200) {
      Map<String, dynamic> data = json.decode(httpResponse.body);
      ItemsResponse response = new ItemsResponse.fromJson(data);
      if (response.items.isNotEmpty) {
        return {
          'gtin_cd': response.items[0].ean,
          'gtin_nm': response.items[0].title
        };
      }
    } else {
      print(httpResponse.statusCode);
      print(statusCodes[httpResponse.statusCode]);
      return null;
    }
  }

  // code can be UPC, ISBN or EAN
  Future<ItemsResponse> lookup(String code, http.Client client) async {
    String url = trial ? trialUrl : paidUrl + "/lookup?upc=$code";
    return httpGet(url, client);
  }

  Future<ItemsResponse> search(String searchRequest, http.Client client) async {
    String url = trial ? trialUrl : paidUrl + "/lookup?s=$searchRequest";
    return httpGet(url, client);
  }

  Future<ItemsResponse> httpGet(String url, http.Client client) async {
    http.Response httpResponse =
        await client.get(url, headers: {"Accept": "application/json"});
    if (httpResponse.statusCode == 200) {
      Map<String, dynamic> data = json.decode(httpResponse.body);
      return ItemsResponse.fromJson(data);
    } else {
      print(httpResponse.statusCode);
      print(statusCodes[httpResponse.statusCode]);
      return null;
    }
  }
}

class ItemsResponse {
  final String code;
  final int total;
  final int offset;
  final List<Item> items;

  ItemsResponse(this.code, this.total, this.offset, this.items);

  factory ItemsResponse.fromJson(Map<String, dynamic> json) {
    return new ItemsResponse(json['code'], json['total'], json['offset'],
        json['items'].map((item) => new Item.fromJson(item)).toList());
  }
}

class Item {
  final String ean;
  final String title;
  final String description;
  final String brand;
  final String model;
  final String color;
  final String size;
  final String dimension;
  final String weight;

  var lowestRecordedPrice;
  var highestRecordedPrice;

  String currency;
  String upc;
  String gtin;
  String elid;
  String userData;

  List<String> images;
  List<Offer> offers;

  Item(this.ean, this.title, this.description, this.brand, this.model,
      this.color, this.size, this.dimension, this.weight);

  factory Item.fromJson(Map<String, dynamic> json) {
    Item item = new Item(
        json['ean'],
        json['title'],
        json['description'],
        json['brand'],
        json['model'],
        json['color'],
        json['size'],
        json['dimension'],
        json['weight']);

    if (json.containsKey('lowest_recorded_price')) {
      item.lowestRecordedPrice = json['lowest_recorded_price'];
    }
    if (json.containsKey('highest_recorded_price')) {
      item.highestRecordedPrice = json['highest_recorded_price'];
    }
    if (json.containsKey('currency')) {
      item.currency = json['currency'];
    }
    if (json.containsKey('upc')) {
      item.upc = json['upc'];
    }
    if (json.containsKey('gtin')) {
      item.gtin = json['gtin'];
    }
    if (json.containsKey('elid')) {
      item.elid = json['elid'];
    }
    if (json.containsKey('user_data')) {
      item.userData = json['user_data'];
    }
    if (json.containsKey('images')) {
      item.images = json['images'].toList();
    }
    if (json.containsKey('offers')) {
      item.offers =
          json['offers'].map((offer) => new Offer.fromJson(offer)).toList();
    }

    return item;
  }
}

class Offer {
  final String merchant;
  final String domain;
  final String title;
  final String currency;
  final String shipping;
  final String condition;
  final String availability;
  final String link;
  final int updatedTime;
  final listPrice;
  final price;

  Offer(
      this.merchant,
      this.domain,
      this.title,
      this.currency,
      this.shipping,
      this.condition,
      this.availability,
      this.link,
      this.listPrice,
      this.price,
      this.updatedTime);

  factory Offer.fromJson(Map<String, dynamic> json) {
    return new Offer(
        json['merchant'],
        json['domain'],
        json['title'],
        json['currency'],
        json['shipping'],
        json['condition'],
        json['availability'],
        json['link'],
        json['list_price'],
        json['price'],
        json['updated_t']);
  }
}

Map<int, Map<String, String>> statusCodes = {
  400: {
    "code": "INVALID_QUERY",
    "description": "Required parameters is missing in the request."
  },
  401: {
    "code": "AUTH_ERR",
    "description":
        "For paid plan only, user_key in the request header can not be authenticated.",
  },
  404: {
    "code": "NOT_FOUND",
    "description": "No matched item was found or wrong endpoint path.",
  },
  429: {
    "code": "EXCEED_LIMIT",
    "description":
        "Exceed daily request limits.Please check your Plan for the daily limits. The X-RateLimit-Limit in the response header also indicates your daily request limit.",
  },
  500: {
    "code": "SERVER_ERR",
    "description":
        "Our bad.If the problem persists, please shoot us an email with your sample request and the time it happened."
  }
};
