import 'package:money/money.dart';

class Product {
  final String name;
  final String id;
  final Money price;
  final String barcode;
  String priceString;
  int milliliter;
  int gram;
  String info = 'Info nicht verfügbar.';
  String imgUrl;

  Product(this.barcode, this.id, this.name, this.price) {
    this.priceString = _buildMoneyString(price);
  }

  bool hasImg() => imgUrl != null;

  void setLiquid(int ml) {
    milliliter = ml;
    final double liter = milliliter / 1000;
    info = '$liter l';
  }

  void setSolid(int g) {
    gram = g;
    final double kilos = gram / 1000;
    info = '$kilos kg';
  }

  void setImgUrl(String img) {
    imgUrl = img;
  }

  factory Product.fromJson(Map<String, dynamic> json, String docID) {
    final price = json['price'];
    final priceInCents = price['in_cents'];
    final currencyName = price['currency'];

    final gtinNm = json['gtin_nm'];
    final ml = json['m_ml'];
    final g = json['m_g'];
    final img = json['gtin_img'];
    final code = json['gtin_code'];

    if (gtinNm is String &&
        docID is String &&
        priceInCents is int &&
        currencyName is String) {
      Product result = new Product(code, docID, gtinNm,
          new Money(priceInCents, new Currency(currencyName)));
      if (ml is int) {
        result.setLiquid(ml);
      } else if (g is int) {
        result.setSolid(g);
      }
      if (img is String) {
        result.setImgUrl(img);
      }
      return result;
    }
    return null;
  }

  String _buildMoneyString(Money price) {
    if (price == null) return '0,00';
    final int myInteger = price.amount ~/ 100;
    final int myFraction = price.amount % 100;
    final String mySymbol = _currencyToSymbol(price.currency.code);

    return myFraction < 10
        ? '$myInteger,0$myFraction $mySymbol'
        : '$myInteger,$myFraction $mySymbol';
  }

  final Map<String, String> moneySymbols = {
    'EUR': '€',
    'USD': '\$',
    'CHF': 'Fr',
    'GBP': '£',
    'SEK': 'kr',
    'NOK': 'kr',
    'JPY': '¥',
    'CNY': '元'
  };

  String _currencyToSymbol(String isoCurrency) {
    isoCurrency = isoCurrency.toUpperCase();
    if (isoCurrency.length == 3) {
      if (moneySymbols.containsKey(isoCurrency)) {
        return moneySymbols[isoCurrency];
      } else {
        print("Currency symbol unknown");
      }
    } else {
      print("Not a valid ISO currency");
    }
    return null;
  }
}

class Shop {
  final String name;
  final String address;
  final String placeId;

  Shop({this.name, this.address, this.placeId});

  factory Shop.fromJson(Map<String, dynamic> json) {
    return new Shop(
        name: json['name'],
        address: json['vicinity'],
        placeId: json['place_id']);
  }

  Map<String, dynamic> toJson() {
    return {"name": name, "vicinity": address, "place_id": placeId};
  }
}

class Einkauf {
  Shop shop;
  List<Product> cart;
  Money value;

  //TODO: Use this class in main
}
