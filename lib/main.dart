import 'dart:async';
import 'dart:core';
import 'dart:math';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:money/money.dart';

import './src/lib/itemdb.dart';
import './src/pages/shops.dart';
import './src/pages/signin.dart';

const moneySymbols = {
  'EUR': '€',
  'USD': '\$',
  'CHF': 'Fr',
  'GBP': '£',
  'SEK': 'kr',
  'NOK': 'kr',
  'JPY': '¥',
  'CNY': '元'
};

String currencyToSymbol(String isoCurrency) {
  isoCurrency = isoCurrency.toUpperCase();
  if (isoCurrency.length == 3) {
    if (moneySymbols.containsKey(isoCurrency)) {
      return moneySymbols[isoCurrency];
    } else {
      print("Currency symbol unknown");
    }
  } else {
    print("Not a valid ISO currency");
  }
  return null;
}

final FirebaseAuth _auth = FirebaseAuth.instance;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Einkaufen',
      theme: new ThemeData(
        primarySwatch: Colors.green,
        accentColor: Colors.red,
        brightness: Brightness.light,
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values
  // provided by the parent (in this case the App widget) and used by the build
  // method of the State. Fields in a Widget subclass are always marked "final".
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Money _valueOfCart = new Money(0, new Currency('EUR'));
  FirebaseUser _user;
  String currentShop = 'default';

  @override
  Widget build(BuildContext context) {
    _updateUser();

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
        appBar:
            new AppBar(leading: new MenuButton(), title: new Text('Einkaufen')),
        drawer: _buildDrawer(context),
        floatingActionButton: new FloatingActionButton(
          onPressed: scan,
          tooltip: 'Neuen Artikel hinzufügen',
          child: new Icon(Icons.camera_alt),
          backgroundColor: Colors.orangeAccent,
        ),
        body: _buildBody(context));
  }

  Container _buildBody(BuildContext context) {
    final moneyString = buildMoneyString(_valueOfCart);
    return new Container(
      margin: const EdgeInsets.all(8.0),
      child: new Column(children: <Widget>[
        new Container(
          margin: const EdgeInsets.symmetric(vertical: 32.0),
          child: new Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Text(
                'Aktuelle Summe:',
                style: new TextStyle(fontSize: 18.0),
              ),
              new Padding(
                child: new Text(
                  moneyString,
                  style: new TextStyle(fontSize: 36.0),
                ),
                padding: const EdgeInsets.symmetric(vertical: 16.0),
              ),
              new PayButton(_valueOfCart.amount),
            ],
          ),
        ),
        new Container(
          margin: const EdgeInsets.symmetric(horizontal: 16.0),
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Text("Konsum, Universitätsstraße 16"),
              new IconButton(icon: new Icon(Icons.filter_list), onPressed: null)
            ],
          ),
        ),
        new Expanded(
          child: buildShoppingCart(),
        ),
      ]),
    );
  }

  Drawer _buildDrawer(BuildContext context) {
    return new Drawer(
        child: new ListView(
      children: <Widget>[
        buildDrawerHeader(context),
        new ListTile(
          leading: const Icon(Icons.home),
          title: new Text('Aktueller Einkauf'),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        new ListTile(
          leading: const Icon(Icons.shopping_basket),
          title: new Text('Vergangene Einkäufe'),
          onTap: () {},
        ),
        new ListTile(
          leading: const Icon(Icons.location_on),
          title: new Text('Shops'),
          onTap: () {
            Navigator
                .of(context)
                .push(new MaterialPageRoute(builder: (context) {
              return new Shops();
            }));
          },
        ),
        new ListTile(
          leading: const Icon(Icons.monetization_on),
          title: new Text('Bilanz'),
          onTap: () {},
        ),
        new Divider(),
        new ListTile(
          leading: const Icon(Icons.settings),
          title: new Text('Einstellungen'),
          onTap: () {},
        ),
      ],
    ));
  }

  UserAccountsDrawerHeader buildDrawerHeader(BuildContext context) {
    bool isGuest = true;
    if (_user != null) {
      if (!_user.isAnonymous) {
        isGuest = false;
      }
    }
    return new UserAccountsDrawerHeader(
      accountName: new Text(isGuest ? 'Willkommen!' : _user.displayName),
      accountEmail:
          new Text(isGuest ? 'Sie sind als Gast angemeldet.' : 'Abmelden'),
      currentAccountPicture: new CircleAvatar(
        backgroundColor: Colors.orangeAccent,
        child: new Text(
          isGuest ? 'G' : _user.displayName.substring(0, 1).toUpperCase(),
          style: new TextStyle(color: Colors.white),
        ),
      ),
      onDetailsPressed: () {
        if (isGuest) {
          Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
            return new SignInPage();
          }));
        } else {
          _signOut();
        }
      },
    );
  }

  Widget buildShoppingCart() {
    if (_user == null) {
      return new Center(child: new CircularProgressIndicator());
    }

    final CollectionReference cartRef = Firestore.instance
        .collection('user')
        .document(_user.uid)
        .collection('cart');

    return new StreamBuilder(
      stream: cartRef.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) {
          // By default, show a loading spinner
          return new Center(child: new CircularProgressIndicator());
        }

        Money tempPrice = new Money(0, new Currency('EUR'));

        ListView cart = new ListView(
          children:
              snapshot.data.documents.map<Widget>((DocumentSnapshot document) {
            MyItem thisItem = MyItem.fromSnapshot(document);
            if (thisItem != null) {
              tempPrice += thisItem.price;
            }
            return _buildCartItem(thisItem);
          }).toList(),
        );

        _valueOfCart = tempPrice;
        return cart;
      },
    );
  }

  Widget _buildCartItem(MyItem item) {
    if (item == null) {
      return ListTile(
        title: new Text('Ungültiges Produkt'),
        trailing: new RemoveFromCartButton(item.id, _removeItemFromCart),
      );
    }

    return new ListTile(
      leading: new CircleAvatar(
        child: item.hasImg()
            ? null
            : new Text(item.name.substring(0, 1).toUpperCase()),
        backgroundImage: item.hasImg() ? new NetworkImage(item.imgUrl) : null,
        backgroundColor: Colors.green,
      ),
      title: new Text(item.name),
      subtitle: new Text(item.info),
      trailing: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: new Text(
              item.priceString,
              style: new TextStyle(fontSize: 18.0),
            ),
          ),
          new RemoveFromCartButton(item.id, _removeItemFromCart),
        ],
      ),
    );
  }

  Future<Null> _addItem(String barcode) async {
    print(barcode);
    final String uid = _user.uid;
    DocumentSnapshot productData;
    Map<String, dynamic> product;

    CollectionReference prodRef = Firestore.instance
        .collection('shop')
        .document(this.currentShop)
        .collection('product');

    CollectionReference scanRef =
        Firestore.instance.collection('user').document(uid).collection('scan');

    CollectionReference cartRef =
        Firestore.instance.collection('user').document(uid).collection('cart');

    try {
      productData = await prodRef.document(barcode).get();
    } catch (PlatformException) {
      Scaffold.of(context).showSnackBar(
          new SnackBar(content: new Text("Keine Internetverbindung.")));
      print("Platform Exception. Keine Internetverbindung?");
    }

    if (productData == null || !productData.exists) {
      final db = new UpcItemDB();
      final data = await db.lookup(barcode, new http.Client());
      if (data != null) {
        Item item = data.items[0];
        product = {
          'gtin_nm': item.title,
          'gtin_cd': item.ean,
        };
      } else {
        print('product unknown');
        product = {
          'gtin_nm': 'Kommt bald ;)',
          'gtin_cd': '000000000',
        };
      }
    } else {
      product = productData.data;
      print('product found');
    }

    var rng = new Random();
    int priceInCents = rng.nextInt(1000);
    // int priceInCents = 124;
    product['price'] = {'in_cents': priceInCents, 'currency': 'EUR'};

    print(product);
    cartRef.document().setData(product);
    scanRef.document().setData({'barcode': barcode, 'shop': this.currentShop});
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      _addItem(barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        print('Kamerazugriff nicht erlaubt.');
      } else {
        throw e;
      }
    } on FormatException {
      // User returned using the "back"-button before scanning anything.
      // Do Nothing
    } catch (e) {
      print('Unbekannter Fehler: $e');
    }
  }

  Future<Null> _removeItemFromCart(String docID) async {
    final uid = _user.uid;
    await Firestore.instance
        .collection('user')
        .document(uid)
        .collection('cart')
        .document(docID)
        .delete();
  }

  Future<Null> _updateUser() async {
    if (_user == null) {
      await _auth.signInAnonymously();
    }
    final FirebaseUser user = await _auth.currentUser();
    setState(() {
      _user = user;
    });
  }

  Future<Null> _signOut() async {
    await _auth.signOut();
    await _updateUser();
  }
}

class RemoveFromCartButton extends StatelessWidget {
  final String _documentID;
  final removeItemFromCart;
  const RemoveFromCartButton(this._documentID, this.removeItemFromCart,
      {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new IconButton(
      icon: const Icon(Icons.remove_shopping_cart),
      onPressed: () {
        removeItemFromCart(_documentID);
      },
    );
  }
}

class MenuButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new IconButton(
      icon: new Icon(Icons.menu),
      onPressed: () {
        Scaffold.of(context).openDrawer();
      },
    );
  }
}

class PayButton extends StatelessWidget {
  final int amount;
  const PayButton(this.amount, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      color: Colors.redAccent,
      textColor: Colors.white,
      disabledColor: Colors.grey.shade300,
      disabledTextColor: Colors.grey.shade700,
      child: new Text(
        'Bezahlen',
        style: new TextStyle(fontSize: 18.0),
      ),
      onPressed: amount == 0 ? null : () => checkout(context),
    );
  }
}

String buildMoneyString(Money price) {
  if (price == null) return '0,00';
  final int myInteger = price.amount ~/ 100;
  final int myFraction = price.amount % 100;
  final String mySymbol = currencyToSymbol(price.currency.code);

  return myFraction < 10
      ? '$myInteger,0$myFraction $mySymbol'
      : '$myInteger,$myFraction $mySymbol';
}

Future<Null> checkout(BuildContext context) async {
  // TODO: push checkout page
  Scaffold
      .of(context)
      .showSnackBar(new SnackBar(content: new Text('Einkauf bezahlt.')));
}

class MyItem {
  final String name;
  final String id;
  final Money price;
  final String barcode;
  String priceString;
  int milliliter;
  int gram;
  String info = 'Info nicht verfügbar.';
  String imgUrl;

  MyItem(this.barcode, this.id, this.name, this.price) {
    this.priceString = buildMoneyString(price);
  }

  bool hasImg() => imgUrl != null;

  void setLiquid(int ml) {
    milliliter = ml;
    final double liter = milliliter / 1000;
    info = '$liter l';
  }

  void setSolid(int g) {
    gram = g;
    final double kilos = gram / 1000;
    info = '$kilos kg';
  }

  void setImgUrl(String img) {
    imgUrl = img;
  }

  factory MyItem.fromSnapshot(DocumentSnapshot document) {
    final docID = document.documentID;
    final price = document['price'];
    final priceInCents = price['in_cents'];
    final currencyName = price['currency'];

    final gtinNm = document['gtin_nm'];
    final ml = document['m_ml'];
    final g = document['m_g'];
    final img = document['gtin_img'];
    final code = document['gtin_code'];

    if (gtinNm is String &&
        docID is String &&
        priceInCents is int &&
        currencyName is String) {
      MyItem result = new MyItem(code, docID, gtinNm,
          new Money(priceInCents, new Currency(currencyName)));
      if (ml is int) {
        result.setLiquid(ml);
      } else if (g is int) {
        result.setSolid(g);
      }
      if (img is String) {
        result.setImgUrl(img);
      }
      return result;
    }
    return null;
  }
}
